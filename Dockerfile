FROM centos:7
#MAINTAINER corey@logicminds.biz
RUN mkdir -p /scripts
COPY scripts/rvm_setup.sh /scripts/rvm_setup.sh
COPY scripts/centos_7_x.sh /scripts/centos_7_x.sh
RUN bash /scripts/centos_7_x.sh
RUN bash /scripts/rvm_setup.sh
