#!/usr/bin/env bash

curl -sSL https://rvm.io/mpapis.asc | gpg --import -
curl -L get.rvm.io | bash -s stable

source /etc/profile.d/rvm.sh

rvm install 2.2.4
rvm use 2.2.4 --default
rvm rubygems current
gem install bundler

